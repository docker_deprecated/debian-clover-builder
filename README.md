# debian-clover-builder

#### [debian-x64-clover-builder](https://hub.docker.com/r/forumi0721debianx64build/debian-x64-clover-builder/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721debianx64build/debian-x64-clover-builder/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721debianx64build/debian-x64-clover-builder/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721debianx64build/debian-x64-clover-builder/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721debianx64build/debian-x64-clover-builder)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721debianx64build/debian-x64-clover-builder)



----------------------------------------
#### Description

* Distribution : [Debian GNU/Linux](https://www.debian.org/)
* Architecture : x64
* Appplication : [CloverBootloader](https://github.com/CloverHackyColor/CloverBootloader/)
    - Bootloader for macOS, Windows and Linux in UEFI and in legacy mode.



----------------------------------------
#### Run

* Nothing



----------------------------------------
#### Usage

* Nothing



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

